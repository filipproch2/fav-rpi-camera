__Update package list from repository (check whether there are new)__
```
sudo apt-get update
```
__Update packages in system (previous command necessary)__
```
sudo apt-get upgrade & sudo apt-get dist-upgrade
```
__Clear the terminal window / screen__
```
clear
```
__Edit a file on the system__
```
nano PATH_TO_FILE
```
__Shutdown the RPi__
```
sudo halt
```
or
```
sudo poweroff
```
__Reboot the RPi__
```
sudo reboot
```
__Open RPi configuration utility__
```
sudo raspi-config
```
__Start graphical interface (GUI)__
```
startx
```
__Check network status__
```
ifconfig
```
__List connected USB devices__
```
lsusb
```