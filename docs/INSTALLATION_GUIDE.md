### NOOBS

Official video tutorial from Raspberry Pi foundation, to proceed with NOOBS installation
https://www.raspberrypi.org/help/noobs-setup/

### Raspi-config

 1. disable serial terminal (ADVANCED -> SERIAL -> NO)
 2. enable camera (right on the first screen)

__reboot system after existing the utility__

### Packages to install

```
sudo apt-get install python-serial
```
```
sudo apt-get install python-picamera
```