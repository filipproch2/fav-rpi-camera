## How to download this repository

Move to the direcory where you want the script to be downloaded (a directory named "fav-rpi-camera" will be created there)
```
git clone https://jacktech24@bitbucket.org/jacktech24/fav-rpi-camera.git
```

## Useful git commands
**All these commands must be executed from "fav-rpi-camera" directory**

##### Update to latest version from Git repository
**!!! changes will be overwritten !!!**
```
git merge origin/master
```
