
## Tested software

 * Python 2.7
 * Raspbian Jena (latest version from http://raspberrypi.org)

## Documentation pages

 * [Installation guide](docs/INSTALLATION_GUIDE.md)
 * [Useful linux commands](docs/LINUX_COMMANDS.md)
 * [Git tutorial/commands](docs/GIT_HELP.md)

## Other resources

camera options:
https://www.raspberrypi.org/documentation/usage/camera/python/README.md

serial communication explained:
 http://www.elinux.org/Serial_port_programming
 http://elinux.org/RPi_Serial_Connection

GPIO control:
 https://www.raspberrypi.org/learning/python-quick-reaction-game/worksheet/

Startup:
 http://www.instructables.com/id/Raspberry-Pi-Launch-Python-script-on-startup/