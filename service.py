import sys, time, os, datetime
import picamera
import RPi.GPIO as GPIO
import serial
import threading
import ConfigParser

__author__ = 'jacktech24'

try:
    config = ConfigParser.RawConfigParser()
    config.read('config')

    img_period = config.getint('ImageSection', 'period') / 1000
    img_storage_dir = str(config.get('ImageSection', 'storage_dir'))
    image_name_tmp = str(config.get('ImageSection', 'name_template'))

    sensor_read_period = config.getint('GeneralSection', 'sensor_read_period')/1000
except:
    print 'error reading configuration file'
    exit(1)


camera = picamera.PiCamera()


try:
    serial_port = serial.Serial(config.get('GeneralSection', 'serial_port'),
                                baudrate=config.getint('GeneralSection', 'serial_baudrate'), timeout=3.0)
    print 'Serial port open'
except:
    print 'Failed to open serial port, are you root?'


def take_image():
    result_name = image_name_tmp.replace('{timestamp}', str(time.time()))
    result_name = result_name.replace('{date}', str(datetime.datetime.now().isoformat()))
    camera.capture(img_storage_dir + "/" + result_name + '.jpg')
    print 'image saved', result_name


def read_sensors():
    # read GPIO values, whatever
    pass


def process_serial_input():
    while True:
        # modify to comply with serial communication of target device
        line = serial_port.readline()
    pass


class ImagingThread(threading.Thread):
    def __init__(self, event):
        threading.Thread.__init__(self)
        self.stopped = event

    def run(self):
        print 'imaging thread started...'
        while not self.stopped.wait(img_period):
            take_image()


class SensorThread(threading.Thread):
    def __init__(self, event):
        threading.Thread.__init__(self)
        self.stopped = event

    def run(self):
        print 'sensor thread started...'
        while not self.stopped.wait(sensor_read_period):
            read_sensors()


class SerialProcessingThread(threading.Thread):
    def __init__(self, event):
        threading.Thread.__init__(self)
        self.stopped = event

    def run(self):
        print 'serial processing thread started...'
        while not self.stopped:
            process_serial_input()


stopFlag = threading.Event()
imagingThread = ImagingThread(stopFlag)
imagingThread.start()

sensorThread = SensorThread(stopFlag)
sensorThread.start()

serialThread = SerialProcessingThread(stopFlag)
serialThread.start()

try:
    while True:
        time.sleep(5)
except (KeyboardInterrupt, SystemExit):
    print 'exiting...'
    stopFlag.set()
    exit(0)
